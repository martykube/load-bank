# Load Bank Relay Driver

Software for the Beaglebone on the load bank


## Interface

The load bank is a ROS node.  The node listens for messages for the load percentage set point (0-100):

    /load_bank/command_load_percent (std_msgs/UInt8)

The node publishes the current load percentage to:

    /load_bank/load_percent (load_bank/Load)

The [Load](msg/Load.msg) message has a count of running pins, current and voltage attributes.

Send set points with a frequency of at least 1 Hz.  Otherwise, the load bank will declare a heartbeat loss and turn the load off.


If you are connected to the Beaglebone over USB the IP address is 198.162.7.2.


## Running

Log onto the Beaglebone over USB.

    ssh ubuntu@192.168.7.2

The password is temppwd.  Root up and source the ROS setup file for each new terminal. 

    sudo su - 
    source ~/catkin_ws/devel/setup.sh

Start roscore

    roscore

Start the load-bank-service in another termial

    rosrun load_bank load_bank_node.py

Fire up the UI in another terminal

    ./catkin_ws/src/load_bank/scripts/load_bank_ui.py

And you should have a UI working.  Use the arrow keys.

 
## Installing


Create a ROS workspace, check out and build the project, and configure your workspace.  The UI node doesn not have to run under root.   The driver node does have to run as root as it uses GPIO and ADC facilities.

    mkdir -p ~/catkin_ws/src
    cd ~/catkin_ws/src/
    catkin_init_workspace 
    git clone git@gitlab.com:NL-outback-challenge-2016/load-bank.git load_bank
    cd ~/catkin_ws
    catkin_make
    source devel/setup.sh












