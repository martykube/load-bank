#!/usr/bin/env python


# ROS Driver for the load bank.
#
# Topics Subscribed:
#      /load-bank/command-load-percent (load_bank/Load) 
#
# Topics Published:
#      /load-bank/load_percent (std_msgs/UInt8) 


import Queue
import os
import sys

import rospy
from std_msgs.msg import UInt8
from load_bank.msg import Load

import Adafruit_BBIO.GPIO as GPIO
import Adafruit_BBIO.ADC as ADC


global idle_queue
global working_queue
global pins
global heartbeat


pins = [
    'P8_7', 'P8_8', 'P8_9', 'P8_10', 'P8_11',
    'P8_12', 'P8_13', 'P8_14', 'P8_15', 'P8_16',
    'P8_17', 'P8_18',  
    'P9_11', 'P9_12',  'P9_13', 'P9_15', 'P9_16', 
    'P9_23', 'P9_24', 'P9_27'
]


def getNodeName():
    return 'load_bank'


def set_running_pins(num_to_run):

    num_running = working_queue.qsize()
    delta = num_to_run - num_running
    rospy.loginfo("coils to run: %s" % num_to_run)
    rospy.loginfo("coils running: %s" % num_running)
    rospy.loginfo("delta: %s" % delta)
    
    if delta > 0:
        for p in range(delta):
            pin = idle_queue.get_nowait()
            rospy.loginfo("Turn on: %s" % pin)
            GPIO.output(pin, GPIO.LOW)
            working_queue.put_nowait(pin)

    if delta < 0:
        for p in range(abs(delta)):
            pin = working_queue.get_nowait()
            rospy.loginfo("Turn off: %s" % pin)
            GPIO.output(pin, GPIO.HIGH)
            idle_queue.put_nowait(pin)
        

def set_load_cmd(msg):

    global heartbeat 

    rospy.loginfo(rospy.get_caller_id() + " command_load_percent %s" % msg.data)

    # clamp to [0-100]
    power_cmd = int(msg.data)
    power_cmd = max(power_cmd, 0)
    power_cmd = min(power_cmd, 100)

    num_to_run = int(round(float(power_cmd) *  (float(len(pins)) / 100.0)))
    set_running_pins(num_to_run)
    heartbeat = rospy.Time.now()
    

if __name__ == '__main__':
 
    global working_queue
    global idle_queue
    global heartbeat

    rospy.init_node('load_bank')

    # Setup pins for gpio and emit high
    ADC.setup()
    for pin in pins:
        GPIO.setup(pin, GPIO.OUT)
        GPIO.output(pin, GPIO.HIGH)

    # Place all pins in the idle queue
    idle_queue = Queue.Queue(maxsize = len(pins))
    working_queue = Queue.Queue(maxsize = len(pins))
    for pin in pins:
        idle_queue.put_nowait(pin)


    # listen for inboud load command
    rospy.Subscriber(getNodeName() + '/command_load_percent', UInt8, set_load_cmd)

    # publish current power and check heartbeat 
    rospy.loginfo("Starting...")
    pub = rospy.Publisher(getNodeName() + '/load_percent', Load, queue_size=10)
    rate = rospy.Rate(1)
    heartbeat = rospy.Time.now()
    while not rospy.is_shutdown():
        pin_count = working_queue.qsize()
        # P9_39 and P9_40
        current_amps = ADC.read('AIN0')
        voltage_volts = ADC.read('AIN1')
        rospy.logdebug(rospy.get_caller_id() + " pin_count %s current %s voltage %s" % 
                       (pin_count, current_amps, voltage_volts))
        pub.publish(Load(pin_count, current_amps, voltage_volts))
        if (rospy.Time.now() - heartbeat).secs > 2:
            rospy.loginfo("Heartbeat timed out")
            set_running_pins(0)
            pin_count = 0
        rate.sleep()
